package br.edu.up.ws;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class ClienteWS {

  public static void main(String[] args) throws Exception {

    //this.url = "jdbc:mysql://127.11.95.130:3306/" + banco
    //    + "?user=adminkAtUMlH&password=Tvf9qgLYc2mU";
     URL url = new URL("http://localhost:9090/ws/pessoa/listar");
     HttpURLConnection con = (HttpURLConnection)
     url.openConnection();
     con.setRequestMethod("GET");
     con.setRequestProperty("Accept", "application/json");
     //con.setRequestProperty("Accept", "application/json");
     con.connect();
    
     InputStream is = con.getInputStream();
     Scanner leitor = new Scanner(is);
     String texto = leitor.nextLine();
     System.out.println(texto);
     leitor.close();

//    String url = "http://localhost:9090/ws/pessoa/listar";
//    HttpGet get = new HttpGet(url);
//    get.setHeader("Accept", "application/xml");
//    get.setHeader("Accept", "application/json");
//
//    HttpClient client = HttpClientBuilder.create().build();
//    HttpResponse response = client.execute(get);
//    HttpEntity entity = response.getEntity();
//    InputStream is = entity.getContent();
//    Scanner leitor = new Scanner(is);
//    String txt = leitor.nextLine();
//    System.out.println(txt);
//    leitor.close();
  }
}