package br.edu.up.ws;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/ws")
public class WebServices extends ResourceConfig {
	
    public WebServices() {
        packages("br.edu.up.ws");
    }  
}