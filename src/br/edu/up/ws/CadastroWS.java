package br.edu.up.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.edu.up.db.BancoDeDados;
import br.edu.up.db.Conector;
import br.edu.up.db.MySQLConector;
import br.edu.up.dominio.Pessoa;

@Path("pessoa")
public class CadastroWS {
  
  public static BancoDeDados db;
  
  public CadastroWS() {
    // Conex�o MySQL;
    Conector conector = new MySQLConector("cadastro");
    db = new BancoDeDados(conector);
  }
  
	//http://localhost:9090/ws/pessoa/listar
	@GET
	@Path("listar")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML })
	public List<Pessoa> listar() {
	  return db.listarPessoas();
	}
	
  //http://localhost:9090/ws/pessoa/buscar/1	
	@GET
	@Path("buscar/{id}")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML })
	public Response buscar(@PathParam("id") Integer id) {
	  Pessoa pessoa = db.buscarPorId(id);
		return Response.ok(pessoa).build();
	}

	//http://localhost:9090/ws/pessoa/salvar
	@POST
	@Path("salvar")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response salvar(Pessoa pessoa) {
	  db.incluirPessoa(pessoa);
		return Response.ok().build();
	}

	//http://localhost:9090/ws/pessoa/atualizar
	@PUT
	@Path("atualizar")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response atualizar(Pessoa pessoa) {
	  db.atualizarPessoa(pessoa);
		return Response.ok().build();
	}

	//http://localhost:9090/ws/pessoa/excluir/1
	@DELETE
	@Path("excluir/{id}")
	public Response excluir(@PathParam("id") Integer id) {
	  db.excluirPessoa(id);
		return Response.ok().build();
	}
}