package br.edu.up.ws;

import java.net.InetSocketAddress;

import javax.ws.rs.ext.RuntimeDelegate;

import com.sun.net.httpserver.HttpHandler; //Classes restritas
import com.sun.net.httpserver.HttpServer; //Classes restritas

@SuppressWarnings("restriction")
public class Servidor {

  public static void main(String[] args) throws Exception {

//    // Conex�o MySQL;
//    Conector conector = new MySQLConector("cadastro");
//    BancoDeDados db = new BancoDeDados(conector);
//    
//    //Read
//    List<Pessoa> pessoas = db.listarPessoas();
//    for (Pessoa pessoa : pessoas) {
//      System.out.println("Nome: " + pessoa.getNome()); 
//    }
    
    // Cria o socket e o server;
    InetSocketAddress isa = new InetSocketAddress(9090);
    HttpServer server = HttpServer.create(isa, 0);

    // Cria o web service e o handler;
    WebServices ws = new WebServices();
    HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(ws, HttpHandler.class);

    // Vincula o handler e inicia o servidor;
    server.createContext("/ws", handler);
    server.start();

    System.out.println("http://localhost:9090/ws rodando...");
  }
}