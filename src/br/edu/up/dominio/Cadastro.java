package br.edu.up.dominio;

import java.util.Date;
import java.util.LinkedHashMap;

public class Cadastro extends LinkedHashMap<Integer, Pessoa> {
	
	private static final long serialVersionUID = 243141777227579977L;
	private static Cadastro singleton;
	
	//singleton
	private Cadastro() {
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setId(1);
		pessoa1.setNome("Jo�o");
		pessoa1.setRg("12345");
		pessoa1.setEstadoCivil("Casado");
		pessoa1.setSexo('M');
		pessoa1.setDataNascimento(new Date());
		pessoa1.setLocal("Curitiba");		
		put(pessoa1.getId(), pessoa1);
	}
	
	public static Cadastro CRUD(){
		if (singleton == null){
			singleton = new Cadastro();
		}
		return Cadastro.singleton;
	}
}