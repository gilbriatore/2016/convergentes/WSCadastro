package br.edu.up.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.edu.up.dominio.Pessoa;

public class BancoDeDados {

  private Conector conector;

  public BancoDeDados(Conector conector) {
    this.conector = conector;
  }

  public List<Pessoa> listarPessoas() {
    
    List<Pessoa> lista = new ArrayList<>();
    
    try {
      Connection con = conector.getConexao();
      Statement exec = con.createStatement();
      ResultSet resultado = exec.executeQuery("select * from pessoas");

      
      while (resultado.next()) {
        Pessoa pessoa = popularPessoa(resultado);
        lista.add(pessoa);
      }

      resultado.close();
      exec.close();
      con.close();
      
    } catch (Exception e) {
      e.printStackTrace();
    }
    return lista;
  }

  public void incluirPessoa(Pessoa pessoa) {
    
    try {
      
      Connection con = conector.getConexao();
      
      String sql = "INSERT INTO pessoas "
          + "(nome, rg, cpf, estado_civil, sexo, data_nasc, local_nasc) "
          + "values(?, ?, ?, ?, ?, ?, ?)";

      PreparedStatement exec = con.prepareStatement(sql);
      
      java.sql.Date data = null;
      if (pessoa.getDataNascimento() != null) {
        data = new java.sql.Date(pessoa.getDataNascimento().getTime());
     }

      exec.setString(1, pessoa.getNome());
      exec.setString(2, pessoa.getRg());
      exec.setString(3, pessoa.getCpf());
      exec.setString(4, pessoa.getEstadoCivil());
      exec.setString(5, String.valueOf(pessoa.getSexo()));
      exec.setDate(6, data);
      exec.setString(7, pessoa.getLocal());
      exec.executeUpdate();

      exec.close();
      con.close();
      
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void atualizarPessoa(Pessoa pessoa) {
    
    try {
      Connection con = conector.getConexao();
      
      String sql = "update pessoas set nome = ?, rg = ?, cpf = ?, estado_civil = ?, "
          + "sexo = ?, data_nasc = ?, local_nasc = ? where id = ?";

      PreparedStatement exec = con.prepareStatement(sql);
      
      java.sql.Date data = null;
      if (pessoa.getDataNascimento() != null) {
        data = new java.sql.Date(pessoa.getDataNascimento().getTime());
     }
      
      exec.setString(1, pessoa.getNome());
      exec.setString(2, pessoa.getRg());
      exec.setString(3, pessoa.getCpf());
      exec.setString(4, pessoa.getEstadoCivil());
      exec.setString(5, String.valueOf(pessoa.getSexo()));
      exec.setDate(6, data);
      exec.setString(7, pessoa.getLocal());
      exec.setInt(8, pessoa.getId());
      
      exec.executeUpdate();

      exec.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Pessoa buscarPorId(int id) {

    Pessoa pessoa = null;
   
    try {
      Connection con = conector.getConexao();
      String sql = "SELECT * FROM Pessoas v WHERE v.id = ?";

      PreparedStatement exec = con.prepareStatement(sql);
      exec.setInt(1, id);
      ResultSet resultado = exec.executeQuery();
      
      if (!resultado.isClosed() && resultado.next()) {
        String nome = resultado.getString(2);
        String rg = resultado.getString(3);
        String cpf = resultado.getString(4);
        String estado = resultado.getString(5);
        char sexo = resultado.getString(6).charAt(0);
        Date data = resultado.getTimestamp(7);
        String local = resultado.getString(8);

        pessoa = new Pessoa();
        pessoa.setId(id);
        pessoa.setNome(nome);
        pessoa.setRg(rg);
        pessoa.setCpf(cpf);
        pessoa.setEstadoCivil(estado);
        pessoa.setSexo(sexo);
        pessoa.setDataNascimento(data);
        pessoa.setLocal(local);
      }
      exec.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return pessoa;
  }

  public void excluirPessoa(Pessoa pessoa) throws SQLException, ClassNotFoundException {
    excluirPessoa(pessoa.getId());
  }

  public void excluirPessoa(int id) {
    
    try {
      Connection con = conector.getConexao();
      String sql = "delete from pessoas where id = ?";

      PreparedStatement exec = con.prepareStatement(sql);
      exec.setInt(1, id);
      exec.executeUpdate();
      exec.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public List<Pessoa> buscarPorNome(String pnome) throws SQLException, ClassNotFoundException {
   
    Connection con = conector.getConexao();
    
    String sql = "select * from pessoas where lower(nome) like ?";

    PreparedStatement exec = con.prepareStatement(sql);
    exec.setString(1, "%" + pnome.toLowerCase() + "%");
    ResultSet resultado = exec.executeQuery();

    List<Pessoa> lista = new ArrayList<>();
    while (resultado.next()) {
      Pessoa pessoa = popularPessoa(resultado);
      lista.add(pessoa);
    }
    resultado.close();
    exec.close();
    con.close();
    return lista;
  }

  private Pessoa popularPessoa(ResultSet resultado) throws SQLException {
    
    int id = resultado.getInt(1);
    String nome = resultado.getString(2);
    String rg = resultado.getString(3);
    String cpf = resultado.getString(4);
    String estado = resultado.getString(5);
    char sexo = resultado.getString(6).charAt(0);
    Date data = resultado.getTimestamp(7);
    String local = resultado.getString(8);

    Pessoa pessoa = new Pessoa();
    pessoa.setId(id);
    pessoa.setNome(nome);
    pessoa.setRg(rg);
    pessoa.setCpf(cpf);
    pessoa.setEstadoCivil(estado);
    pessoa.setSexo(sexo);
    pessoa.setDataNascimento(data);
    pessoa.setLocal(local);
    
    return pessoa;
  }
}