package br.edu.up.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HSQLDBConector implements Conector {
	
	private String driver;
	private String url;

	public HSQLDBConector(String banco) {
		this.driver = "org.hsqldb.jdbc.JDBCDriver";
		//jdbc:hsqldb:res/UrnaEletronica;user=sa;ifexits=true";
		this.url = "jdbc:hsqldb:res/" + banco + ";user=sa;ifexits=true";
	}
	
	public Connection getConexao() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		return DriverManager.getConnection(url); 
	}
}